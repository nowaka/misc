<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page session="false"%>
<html>
<head>
	<title>Image uploader</title>
	<!-- Magnific Popup core CSS file -->
	<link rel="stylesheet" href="/resources/css/jquery.magnific.css"> 
	<link rel="stylesheet" href="/resources/css/main.css"> 
	<script	src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="/resources/js/jquery/plugins/jquery.magnific.min.js"></script>
	<script src="/resources/js/jquery/plugins/jquery.validate.min.js"></script>
	<script src="/resources/js/jquery/plugins/jquery.validate.additional-methods.min.js"></script>
	<script src="/resources/js/imageUploaderSingle.js"></script>
</head>
<body>
	<h1>Image uploader</h1>

	<div class="galleryContainer">
	</div>

	<table id="imagesTable"></table>

	<div id="formContainer">
	</div>
	<a id="addAnotherFileButton" class="button">add another file</a>
	<a id="uploadButton" class="button">upload</a>
	<div id="imagesTableFormTemplate" style="visibility: hidden">
		<form method="POST" action="images/uploadSingle">
			<table>
				<tr>
					<td><label for="name">File name</label></td>
					<td></td>
				</tr>
				<tr>
					<td><input name="name" /><input name="file" type="file" /></td>
					<td></td>
				</tr>
				<tr>
					<td><label>Use file name as alt_tag/caption</label><input name="useNameAltAndCaption" type="checkbox"
						value="true" checked="checked" /></td>
					<td><label>Caption</label><input name="caption" disabled/></td>
				</tr>
				<tr>
					<td></td>
					<td><label>Alt tag</label><input name="altTag" disabled/></td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>
