$(document)
		.ready(
				function() {

					$.validator.addMethod('filesize', function(value, element, param) {
					    // param = size (en bytes) 
					    // element = element to validate (<input>)
					    // value = value of the element (file name)
					    return this.optional(element) || (element.files[0].size <= param); 
					});
					
					var toggleHandler = function() {
						var $this = $(this);
						// $this will contain a reference to the checkbox
						if ($this.is(':checked')) {
							// the checkbox was checked
							$this.closest('table').find("[name=caption]").prop(
									"disabled", true);
							$this.closest('table').find("[name=altTag]").prop(
									"disabled", true);
						} else {
							// the checkbox was unchecked
							$this.closest('table').find("[name=caption]").prop(
									"disabled", false);
							$this.closest('table').find("[name=altTag]").prop(
									"disabled", false);
						}
					};

					var copyNameHandler = function() {
						var $this = $(this);
						var nameInput = $this.closest("td").find("[name=name]");
						if (!nameInput.val()) {
							nameInput.val(this.files[0].name);
						}
					};

					var addFormRow = function() {
						var $form = $("#imagesTableFormTemplate form");
						var $clone = $form.clone();

						$clone.validate({
							rules : {
								name : "required",
								caption : {
									required : true
								},
								altTag : {
									required : true
								},
								file: {
									required : function() {
										return $clone.find("[name=name]").val().length > 1;
									},
									accept: "png|jpe?g|gif", 
									filesize: 1048576  }
							},
							messages: { file: "File must be JPG, GIF or PNG and less than 1MB" }
						});

						$clone.appendTo("#formContainer");
						$clone.find("input:checkbox").click(toggleHandler);
						$clone.find("input:file").change(copyNameHandler);
						$clone.on("submit", function(event) {
							$this = $(this);
							var isvalidate=$this.valid();
					        if(!isvalidate)
					        {
					        	// do not perform any action when form is not valid
					            return false;
					        }
					        
							var data = new FormData();
							var file = $this.find("[name=file]")[0];
							data.append('file', file.files[0]);

							var jsonEvent = {
								"name" : $this.find("[name=name]").val(),
								"useNameAltAndCaption" : $this.find(
										"[name=useNameAltAndCaption]").prop(
										'checked'),
								"altTag" : $this.find("[name=altTag]").val(),
								"caption" : $this.find("[name=caption]").val()
							};

							data.append('event', JSON.stringify(jsonEvent));

							$.ajax({
										type : 'POST',
										url : $clone.attr("action"),
										data : data,
										processData : false,
										contentType : false,
										success : function(response) {
											displayImage(response);
											$clone.detach();
											if ($("#formContainer")
													.find("form").size() === 0)
												addFormRow();
											magnificHandler();
										},
										error : function(error) {
											console.log(error);
										}

									});
							// do not submit form in normal way
							return false;
						});
					};

					var displayImage = function(element) {
						console.log("displayImage()");
						console.log(element);
						var container = $(".galleryContainer");
						container
								.append("<a class=\"image-popup-no-margins\" href=\"images/"
										+ element.id
										+ "/content\">"
										+ "<img src=\"images/"
										+ element.id
										+ "/thumbnail\"\></img>" + "</a>");
					};

					var magnificHandler = function() {
						$('.image-popup-no-margins').magnificPopup({
							type : 'image',
							closeOnContentClick : true,
							closeBtnInside : false,
							fixedContentPos : true,
							mainClass : 'mfp-no-margins mfp-with-zoom',
							image : {
								verticalFit : true
							},
							zoom : {
								enabled : true,
								duration : 300
							// don't foget to change the duration also in CSS
							}
						});
					};

					$.getJSON("images", function(data) {
						$.each(data, function(index, element) {
							displayImage(element);
						});
						magnificHandler();
					});

					// add first form when page refreshed
					addFormRow();

					$('.popup-gallery')
							.magnificPopup(
									{
										delegate : 'a',
										type : 'image',
										mainClass : 'mfp-img-mobile',
										gallery : {
											enabled : true,
											navigateByImgClick : true,
											preload : [ 0, 1 ]
										// Will preload 0 - before current, and
										// 1 after the current image
										},
										image : {
											tError : '<a href="%url%">The image #%curr%</a> could not be loaded.',
											titleSrc : function(item) {
												return item.el.attr('title')
														+ '<small>by Marsel Van Oosten</small>';
											}
										}
									});

					$("#uploadButton").click(function() {
						$("#formContainer").find("form").each(function() {
							$(this).submit();

						});
					});

					$("#addAnotherFileButton").click(addFormRow);

				});