package org.nowak.imggallery.controller;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.imageio.ImageIO;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.nowak.imggallery.FileUploadException;
import org.nowak.imggallery.NoSuchResourceException;
import org.nowak.imggallery.domain.ImageEntity;
import org.nowak.imggallery.domain.ImageUploadEvent;
import org.nowak.imggallery.service.ImageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;
/**
 * Rest web service
 * @author Adam
 *
 */
@Controller
@RequestMapping(value = "/images")
public class ImageController {

	private final ImageService imageService;

	@Autowired
	public ImageController(final ImageService imageService) {
		this.imageService = imageService;
	}

	private static final Logger logger = LoggerFactory
			.getLogger(ImageController.class);

	/**
	 * Returns all images
	 */
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ImageEntity>> getImages(Locale locale,
			Model model) {
		logger.debug("Get images");
		return new ResponseEntity<List<ImageEntity>>(imageService.getImages(),
				HttpStatus.OK);
	}

	/**
	 * Returns image by id
	 * 
	 * @throws NoSuchResourceException
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ImageEntity> getImage(Locale locale, Model model,
			@PathVariable final String id) throws NoSuchResourceException {
		logger.debug("Get image by id {}.", id);
		return new ResponseEntity<ImageEntity>(imageService.getImage(id),
				HttpStatus.OK);
	}

	/**
	 * Returns original size image
	 */
	@RequestMapping(value = "/{id}/content", method = RequestMethod.GET)
	public ResponseEntity<byte[]> getImageContent(Locale locale, Model model,
			@PathVariable final String id) throws NoSuchResourceException {
		logger.debug("Get image content by id {}.", id);
		byte[] imageContent = imageService.getImageContent(id);
		HttpHeaders responseHeaders = httpHeaderImageFile(id,
				imageContent.length);
		return new ResponseEntity<byte[]>(imageContent, responseHeaders,
				HttpStatus.OK);
	}

	/**
	 * Returns miniature image
	 */
	@RequestMapping(value = "/{id}/thumbnail", method = RequestMethod.GET)
	public ResponseEntity<byte[]> getImageThumbnail(Locale locale, Model model,
			@PathVariable final String id) throws NoSuchResourceException {
		//TODO - consider generate image during upload/not download
		logger.debug("Get image content by id {}.", id);
		byte[] imageContent = imageService.getImageContent(id);
		imageContent = scale(imageContent, 100, 100);
		// TODO - move logic to the service class
		HttpHeaders responseHeaders = httpHeaderImageFile(id,
				imageContent.length);
		return new ResponseEntity<byte[]>(imageContent, responseHeaders,
				HttpStatus.OK);
	}

	private byte[] scale(byte[] fileData, int width, int height) {
		ByteArrayInputStream in = new ByteArrayInputStream(fileData);
		try {
			BufferedImage img = ImageIO.read(in);
			if (height == 0) {
				height = (width * img.getHeight()) / img.getWidth();
			}
			if (width == 0) {
				width = (height * img.getWidth()) / img.getHeight();
			}
			Image scaledImage = img.getScaledInstance(width, height,
					Image.SCALE_SMOOTH);
			BufferedImage imageBuff = new BufferedImage(width, height,
					BufferedImage.TYPE_INT_RGB);
			imageBuff.getGraphics().drawImage(scaledImage, 0, 0,
					new Color(0, 0, 0), null);

			ByteArrayOutputStream buffer = new ByteArrayOutputStream();

			ImageIO.write(imageBuff, "jpg", buffer);

			return buffer.toByteArray();
		} catch (IOException e) {
			logger.error("Rescale failed", e);
			return null;
		}
	}

	private static HttpHeaders httpHeaderImageFile(
			final String fileName, final int fileSize) {
		String encodedFileName = fileName.replace('"', ' ').replace(' ', '_');

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.setContentLength(fileSize);
		responseHeaders.setContentType(MediaType.parseMediaType("image/jpeg"));
		responseHeaders.add("Content-Disposition", "filename=\""
				+ encodedFileName + '\"');
		return responseHeaders;
	}

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ImageEntity> uploadImage(Locale locale,
			@RequestPart MultipartFile file,
			@RequestParam("event") String eventParam)
			throws FileUploadException {

		ImageUploadEvent event = toEvent(eventParam);
		logger.debug("Upload image {} {}.", event.getName(), event.getCaption());

		try {
			return new ResponseEntity<ImageEntity>(imageService.createImage(
					file.getBytes(), event), HttpStatus.CREATED);
		} catch (IOException e) {
			logger.error("Could not read file", e);
			throw new FileUploadException(e);
		}
	}

	@RequestMapping(value = "/uploadSingle", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ImageEntity> uploadSingleImage(Locale locale,
			@RequestParam MultipartFile file, @RequestParam String event)
			throws FileUploadException {
		logger.debug("eventParam {} {}", file, event);
		ImageUploadEvent eventObj = toEvent(event);

		try {
			return new ResponseEntity<ImageEntity>(imageService.createImage(
					file.getBytes(), eventObj), HttpStatus.CREATED);
		} catch (IOException e) {
			logger.error("Could not read file", e);
			throw new FileUploadException(e);
		}
	}
/*
	@RequestMapping(value = "/uploadMany", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ImageEntity>> uploadImages(Locale locale,
			@RequestParam MultipartFile[] files,
			@RequestParam(value = "events") String events)
			throws FileUploadException {
		// logger.debug("Upload image {}.", image.getFileName());
		logger.debug("Upload images");
		List<ImageUploadEvent> eventsList = toEvents(events);
		List<byte[]> data = new ArrayList<byte[]>();

		try {
			for (int i = 0; i < eventsList.size(); i++) {
				// useFileNameAsAltAndDefault(files[i], eventsList.get(i));
				data.add(files[i].getBytes());
			}

			return new ResponseEntity<List<ImageEntity>>(
					imageService.createImages(data, eventsList),
					HttpStatus.CREATED);
		} catch (IOException e) {
			logger.error("Upload failed", e);
			throw new FileUploadException(e);
		}

	}
*/
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteImage(Locale locale,
			@PathVariable final String id) throws NoSuchResourceException {
		// logger.debug("Upload image {}.", image.getFileName());
		logger.debug("Delete image {}", id);
		imageService.deleteImage(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	private ImageUploadEvent toEvent(String json) throws FileUploadException {
		try {
			return new ObjectMapper().readValue(json, ImageUploadEvent.class);
		} catch (JsonParseException e) {
			logger.error("Upload failed", e);
			throw new FileUploadException(e);
		} catch (JsonMappingException e) {
			logger.error("Upload failed", e);
			throw new FileUploadException(e);
		} catch (IOException e) {
			logger.error("Upload failed", e);
			throw new FileUploadException(e);
		}
	}
/*
	private List<ImageUploadEvent> toEvents(String json)
			throws FileUploadException {
		List<ImageUploadEvent> list;
		try {
			list = new ObjectMapper().readValue(json, TypeFactory
					.collectionType(List.class, ImageUploadEvent.class));
			return list;
		} catch (JsonParseException e) {
			logger.error("Upload failed", e);
			throw new FileUploadException(e);
		} catch (JsonMappingException e) {
			logger.error("Upload failed", e);
			throw new FileUploadException(e);
		} catch (IOException e) {
			logger.error("Upload failed", e);
			throw new FileUploadException(e);
		}
	}
*/
	@ExceptionHandler(NoSuchResourceException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public void handleResourceNotFound() {
		logger.debug("Resource not found");
	}

	@ExceptionHandler(FileUploadException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public void handleUploadException() {
		logger.debug("File upload exception");
	}

}
