package org.nowak.imggallery.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.nowak.imggallery.NoSuchResourceException;
import org.nowak.imggallery.dao.ImageDAO;
import org.nowak.imggallery.domain.ImageEntity;
import org.nowak.imggallery.domain.ImageUploadEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("imageService")
public class ImageServiceImpl implements ImageService {

	private final ImageDAO imageDAO;

	@Autowired
	public ImageServiceImpl(ImageDAO imageDAO) {
		this.imageDAO = imageDAO;
	}

	@Override
	public List<ImageEntity> getImages() {
		return Collections.unmodifiableList(imageDAO.getAll());
	}

	@Override
	public ImageEntity getImage(String id) throws NoSuchResourceException {
		return imageDAO.getById(id);
	}

	// FIXME - this should be done in one transaction
	@Override
	public ImageEntity createImage(byte[] imgData, ImageUploadEvent event) {
		return imageDAO.create(imgData, event);
	}

	@Override
	public void deleteImage(String id) throws NoSuchResourceException {
		imageDAO.delete(id);
	}

	// FIXME - this should be done in one transaction
	@Override
	public List<ImageEntity> createImages(List<byte[]> data,
			List<ImageUploadEvent> events) {
		List<ImageEntity> images = new ArrayList<ImageEntity>();
		for (int i = 0; i < events.size(); i++) {
			images.add(imageDAO.create(data.get(i), events.get(i)));
		}
		return Collections.unmodifiableList(images);
	}

	@Override
	public byte[] getImageContent(String id) throws NoSuchResourceException {
		return imageDAO.getContentById(id);
	}

}
