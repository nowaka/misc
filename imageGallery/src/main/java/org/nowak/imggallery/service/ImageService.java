package org.nowak.imggallery.service;

import java.util.List;

import org.nowak.imggallery.NoSuchResourceException;
import org.nowak.imggallery.domain.ImageEntity;
import org.nowak.imggallery.domain.ImageUploadEvent;

public interface ImageService {

	public List<ImageEntity> getImages();

	public ImageEntity getImage(String id) throws NoSuchResourceException;

	public ImageEntity createImage(byte[] imgData, ImageUploadEvent event);

	public void deleteImage(String id) throws NoSuchResourceException;

	public List<ImageEntity> createImages(List<byte[]> data,
			List<ImageUploadEvent> events);

	public byte[] getImageContent(String id) throws NoSuchResourceException;
}
