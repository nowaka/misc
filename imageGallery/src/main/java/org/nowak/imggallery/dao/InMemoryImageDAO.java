package org.nowak.imggallery.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.nowak.imggallery.NoSuchResourceException;
import org.nowak.imggallery.domain.ImageEntity;
import org.nowak.imggallery.domain.ImageUploadEvent;
import org.springframework.stereotype.Repository;

@Repository("imageDAO")
public class InMemoryImageDAO implements ImageDAO {

	private Map<String, ImageEntity> repository = new ConcurrentHashMap<String, ImageEntity>();
	private Map<String, byte[]> contentRepository = new ConcurrentHashMap<String, byte[]>();

	@Override
	public List<ImageEntity> getAll() {
		return new ArrayList<ImageEntity>(repository.values());
	}

	@Override
	public ImageEntity getById(String id) throws NoSuchResourceException {
		ImageEntity imageEntity = repository.get(id);
		if (imageEntity == null)
			throw new NoSuchResourceException();
		else
			return imageEntity;
	}

	@Override
	public byte[] getContentById(String id) throws NoSuchResourceException {
		byte[] bs = contentRepository.get(id);
		if (bs == null || bs.length == 0)
			throw new NoSuchResourceException();
		else
			return bs;
	}

	@Override
	public ImageEntity create(byte[] imgData, ImageUploadEvent event) {
		String uniqueID = UUID.randomUUID().toString();
		ImageEntity entity = new ImageEntity.Builder().id(uniqueID)
				.altTag(event.getAltTag()).caption(event.getCaption())
				.fileName(event.getName())
				.fileNameAsAlt(event.getUseNameAltAndCaption()).build();
		repository.put(uniqueID, entity);
		contentRepository.put(uniqueID, imgData);
		return entity;
	}

	@Override
	public void delete(String id) throws NoSuchResourceException {
		ImageEntity removed = repository.remove(id);
		if (removed == null)
			throw new NoSuchResourceException();
	}

}
