package org.nowak.imggallery.dao;

import java.util.List;

import org.nowak.imggallery.NoSuchResourceException;
import org.nowak.imggallery.domain.ImageEntity;
import org.nowak.imggallery.domain.ImageUploadEvent;

public interface ImageDAO {

	public List<ImageEntity> getAll();

	public ImageEntity getById(String id) throws NoSuchResourceException;

	public byte[] getContentById(String id) throws NoSuchResourceException;

	public ImageEntity create(byte[] imgData, ImageUploadEvent event);

	public void delete(String id) throws NoSuchResourceException;
}
