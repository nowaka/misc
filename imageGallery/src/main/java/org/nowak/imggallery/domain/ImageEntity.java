package org.nowak.imggallery.domain;

public class ImageEntity {
	private final String id;
	private final String fileName;
	private final Boolean fileNameAsAlt;
	private final String caption;
	private final String altTag;

	// private final byte[] content;

	private ImageEntity(Builder builder) {
		this.id = builder.id;
		this.fileName = builder.fileName;
		this.fileNameAsAlt = builder.fileNameAsAlt;
		this.caption = builder.caption;
		this.altTag = builder.altTag;
		// this.content = builder.content;
	}

	public Boolean getFileNameAsAltAndCaption() {
		return fileNameAsAlt;
	}

	public String getCaption() {
		if (getFileNameAsAltAndCaption() != null
				&& getFileNameAsAltAndCaption())
			return fileName;
		else
			return caption;
	}

	public String getAltTag() {
		if (getFileNameAsAltAndCaption() != null
				&& getFileNameAsAltAndCaption())
			return fileName;
		else
			return altTag;
	}

	public String getId() {
		return id;
	}

	public String getFileName() {
		return fileName;
	}

	// public byte[] getContent() {
	// return content;
	// }

	public static class Builder {
		// private byte[] content;
		private String id;
		private String fileName;
		private Boolean fileNameAsAlt;
		private String caption;
		private String altTag;

		public Builder id(String id) {
			this.id = id;
			return this;
		}

		public Builder fileName(String fileName) {
			this.fileName = fileName;
			return this;
		}

		public Builder fileNameAsAlt(Boolean fileNameAsAlt) {
			this.fileNameAsAlt = fileNameAsAlt;
			return this;
		}

		public Builder caption(String caption) {
			this.caption = caption;
			return this;
		}

		public Builder altTag(String altTag) {
			this.altTag = altTag;
			return this;
		}

		// public Builder content(byte[] content) {
		// this.content = content;
		// return this;
		// }

		public ImageEntity build() {
			return new ImageEntity(this);
		}
	}
}
