package org.nowak.imggallery.domain;

import org.springframework.web.multipart.MultipartFile;

public class SingleImageUploadEvent {

	private String name;
	private Boolean useNameAltAndCaption;
	private String caption;
	private String altTag;
	private MultipartFile file;

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getUseNameAltAndCaption() {
		return useNameAltAndCaption;
	}

	public void setUseNameAltAndCaption(Boolean useNameAltAndCaption) {
		this.useNameAltAndCaption = useNameAltAndCaption;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getAltTag() {
		return altTag;
	}

	public void setAltTag(String altTag) {
		this.altTag = altTag;
	}

}
