package org.nowak.imggallery;

public class FileUploadException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FileUploadException(String message, Throwable cause) {
		super(message, cause);
	}

	public FileUploadException(Throwable cause) {
		super(cause);
	}

}
