package org.nowak.imggallery.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.type.TypeFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.nowak.imggallery.NoSuchResourceException;
import org.nowak.imggallery.domain.ImageEntity;
import org.nowak.imggallery.domain.ImageUploadEvent;
import org.nowak.imggallery.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration()
@ContextConfiguration({ "classpath:spring/root-context-test.xml",
		"classpath:spring/appServlet/servlet-context-test.xml" })
public class ImageControllerTest {

	private static final String JSON = "{\"name\":\"fileName.jpg\"}";
//	private static final String JSON_ARRAY = "[{\"name\":\"fileName1.jpg\"},{\"name\":\"fileName2.jpg\"}]";

	@Autowired
	private WebApplicationContext wac;

	@Autowired
	@Mock
	private ImageService imageService;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = webAppContextSetup(this.wac).build();
	}

	@After
	public void resetMock() {
		// this is needed because we use one mock defined in
		// root-context-test.xml for all test methods
		reset(imageService);
	}

	@Test
	public void getImages() throws Exception {
		String id = "1";
		String fileName = "fileName.jpg";
		ImageEntity image = new ImageEntity.Builder().id(id).fileName(fileName)
				.build();

		Mockito.when(imageService.getImages()).thenReturn(
				Collections.singletonList(image));

		this.mockMvc
				.perform(get("/images").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(
						content().contentTypeCompatibleWith(
								MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$").isArray())
				.andExpect(jsonPath("$[0].id").value("1"))
				.andExpect(jsonPath("$[0].fileName").value("fileName.jpg"));
	}

	@Test
	public void getImage() throws Exception {
		String id = "1";
		String fileName = "fileName.jpg";
		ImageEntity image = new ImageEntity.Builder().id(id).fileName(fileName)
				.build();
		when(imageService.getImage(anyString())).thenReturn(image);

		this.mockMvc
				.perform(
						get("/images/" + id).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(
						content().contentTypeCompatibleWith(
								MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.id").value(id))
				.andExpect(jsonPath("$.fileName").value(fileName));
	}

	@Test
	public void getNotExistingImage() throws Exception {
		String id = "1";
		doThrow(new NoSuchResourceException()).when(imageService).getImage(
				anyString());

		this.mockMvc.perform(
				get("/images/" + id).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}

	@Ignore
	@Test
	public void createImage() throws Exception {
		String id = "1";
		String fileName = "fileName.jpg";
		ImageEntity image = new ImageEntity.Builder().id(id).fileName(fileName)
				.build();
		Mockito.when(
				imageService.createImage(any(byte[].class),
						Mockito.any(ImageUploadEvent.class))).thenReturn(image);

		MockMultipartFile firstFile = new MockMultipartFile("file",
				"filename.txt", "text/plain", "some data".getBytes());
		// MockMultipartFile jsonFile = new MockMultipartFile("event", "",
		// "application/json",
		// "{\"name\":\"fileName.jpg\"}".getBytes());

		this.mockMvc
				.perform(
						fileUpload("/images").file(firstFile).param("events",
								JSON))

				.andExpect(status().isCreated())
				.andExpect(
						content().contentTypeCompatibleWith(
								MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.id").value(id))
				.andExpect(jsonPath("$.fileName").value(fileName));
	}

	@Ignore
	@Test
	public void uploadImage() throws Exception {
		String id = "1";
		String fileName = "fileName.jpg";
		ImageEntity image = new ImageEntity.Builder().id(id).fileName(fileName)
				.build();
		Mockito.when(
				imageService.createImage(any(byte[].class),
						Mockito.any(ImageUploadEvent.class))).thenReturn(image);

		MockMultipartFile firstFile = new MockMultipartFile("file",
				"filename.txt", "text/plain", "some data".getBytes());
		 MockMultipartFile jsonFile = new MockMultipartFile("event", "",
		 "application/json",
		 "{\"name\":\"fileName.jpg\"}".getBytes());

		this.mockMvc
				.perform(
						fileUpload("/images/uploadSingle").file(firstFile).file(jsonFile))

				.andExpect(status().isCreated())
				.andExpect(
						content().contentTypeCompatibleWith(
								MediaType.APPLICATION_JSON));
//				.andExpect(jsonPath("$.id").value(id))
//				.andExpect(jsonPath("$.fileName").value(fileName));
	}
/*	
	@Ignore
	@Test
	public void createImages() throws Exception {
		String id = "1";
		String fileName = "fileName.jpg";
		ImageEntity image = new ImageEntity.Builder().id(id).fileName(fileName)
				.build();
		Mockito.when(
				imageService.createImages(any(List.class), any(List.class)))
				.thenReturn(Collections.singletonList(image));

		MockMultipartFile firstFile = new MockMultipartFile("files",
				"filename1.txt", "text/plain", "some data".getBytes());
		MockMultipartFile secondFile = new MockMultipartFile("files",
				"filename2.txt", "text/plain", "other data".getBytes());
		// MockMultipartFile jsonFile = new MockMultipartFile("events", "",
		// "application/json",
		// "[{\"name\":\"fileName1.jpg\"},{\"name\":\"fileName2.jpg\"}]".getBytes());

		this.mockMvc
				.perform(
						fileUpload("/images/uploadMany").file(firstFile)
								.file(secondFile).param("events", JSON_ARRAY))

				.andExpect(status().isCreated())
				.andExpect(
						content().contentTypeCompatibleWith(
								MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$").isArray())
				.andExpect(jsonPath("$[0].id").value(id))
				.andExpect(jsonPath("$[0].fileName").value(fileName));
	}
*/
	@Test
	public void deleteImage() throws Exception {
		doNothing().when(imageService).deleteImage(anyString());
		this.mockMvc.perform(
				delete("/images/1").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNoContent());
	}

	@Test
	public void deleteNotExistingImage() throws Exception {

		String id = "1";
		doThrow(new NoSuchResourceException()).when(imageService).deleteImage(
				id);

		this.mockMvc.perform(
				delete("/images/" + id).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}

	@Test
	public void testDeser() throws NoSuchResourceException, JsonParseException,
			JsonMappingException, IOException {
		List<ImageUploadEvent> list = new ObjectMapper().readValue(
				"[{\"name\":\"fileName1.jpg\"},{\"name\":\"fileName2.jpg\"}]",
				TypeFactory.collectionType(List.class, ImageUploadEvent.class));
		assertEquals(2, list.size());
	}

}
