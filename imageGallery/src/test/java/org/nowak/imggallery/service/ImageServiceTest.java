package org.nowak.imggallery.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.nowak.imggallery.NoSuchResourceException;
import org.nowak.imggallery.dao.ImageDAO;
import org.nowak.imggallery.domain.ImageEntity;
import org.nowak.imggallery.domain.ImageUploadEvent;
import org.nowak.imggallery.service.ImageService;
import org.nowak.imggallery.service.ImageServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/root-context-test.xml" })
public class ImageServiceTest {

	private static final String ALT_TAG = "altTag";
	private static final boolean FILE_NAME_AS_ALT = false;
	private static final String CAPTION = "example picture";
	private static final String FILE_NAME_JPG = "fileName.jpg";

	@Autowired
	@Mock
	private ImageDAO imageDAO;

	private ImageService imageService;

	@Before
	public void setup() {
		imageService = new ImageServiceImpl(imageDAO);
	}

	@Test
	public void testGetImages() {
		when(imageDAO.getAll()).thenReturn(createImagesTest());
		List<ImageEntity> images = imageService.getImages();
		assertEquals(3, images.size());
		verify(imageDAO, times(1)).getAll();
	}

	@Test
	public void testGetImage() throws NoSuchResourceException {
		when(imageDAO.getById(anyString())).thenReturn(createImageTest());
		ImageEntity entity = imageService.getImage(anyString());
		assertEquals(FILE_NAME_JPG, entity.getFileName());
		assertEquals(CAPTION, entity.getCaption());
		assertEquals(FILE_NAME_AS_ALT, entity.getFileNameAsAltAndCaption());
		assertEquals(ALT_TAG, entity.getAltTag());
		verify(imageDAO, times(1)).getById(anyString());
	}

	@Test
	public void testCreateImage() {
		when(imageDAO.create(any(byte[].class), any(ImageUploadEvent.class)))
				.thenReturn(createImageTest());
		ImageEntity entity = imageService.createImage(any(byte[].class),
				any(ImageUploadEvent.class));
		assertEquals(FILE_NAME_JPG, entity.getFileName());
		assertEquals(CAPTION, entity.getCaption());
		assertEquals(FILE_NAME_AS_ALT, entity.getFileNameAsAltAndCaption());
		assertEquals(ALT_TAG, entity.getAltTag());
		verify(imageDAO, times(1)).create(any(byte[].class),
				any(ImageUploadEvent.class));
	}

	@Test
	public void testDeleteImage() throws NoSuchResourceException {
		doNothing().when(imageDAO).delete(anyString());
		imageService.deleteImage(anyString());
		verify(imageDAO, times(1)).delete(anyString());
	}

	private ImageEntity createImageTest() {
		return new ImageEntity.Builder().fileName(FILE_NAME_JPG)
				.altTag(ALT_TAG).caption(CAPTION)
				.fileNameAsAlt(FILE_NAME_AS_ALT).build();
	}

	private ArrayList<ImageEntity> createImagesTest() {
		ArrayList<ImageEntity> result = new ArrayList<ImageEntity>();
		result.add(new ImageEntity.Builder().build());
		result.add(new ImageEntity.Builder().build());
		result.add(new ImageEntity.Builder().build());
		return result;
	}
}
