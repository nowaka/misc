package org.nowak.imggallery.dao;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.nowak.imggallery.NoSuchResourceException;
import org.nowak.imggallery.dao.ImageDAO;
import org.nowak.imggallery.dao.InMemoryImageDAO;
import org.nowak.imggallery.domain.ImageEntity;
import org.nowak.imggallery.domain.ImageUploadEvent;

public class InMemoryImageDAOTest {

	private static final String ALT_TAG = "altTag";
	private static final boolean FILE_NAME_AS_ALT = false;
	private static final String CAPTION = "example picture";
	private static final String FILE_NAME_JPG = "fileName.jpg";
	private static final byte[] BYTE_ARR = "example text".getBytes();
	ImageDAO imageDAO = new InMemoryImageDAO();

	@Test
	public void testCreate() {
		ImageEntity entity = imageDAO.create(BYTE_ARR, createEvent());
		assertNotNull(entity);
		assertNotNull(entity.getId());
		assertEquals(FILE_NAME_JPG, entity.getFileName());
		assertEquals(CAPTION, entity.getCaption());
		assertEquals(FILE_NAME_AS_ALT, entity.getFileNameAsAltAndCaption());
		assertEquals(ALT_TAG, entity.getAltTag());
	}

	@Test
	public void testGetById() throws NoSuchResourceException {
		ImageEntity entity = imageDAO.create(BYTE_ARR, createEvent());
		assertNotNull(entity);
		assertNotNull(entity.getId());

		entity = imageDAO.getById(entity.getId());

		assertNotNull(entity);
		assertNotNull(entity.getId());
		assertEquals(FILE_NAME_JPG, entity.getFileName());
		assertEquals(CAPTION, entity.getCaption());
		assertEquals(FILE_NAME_AS_ALT, entity.getFileNameAsAltAndCaption());
		assertEquals(ALT_TAG, entity.getAltTag());
	}

	@Test
	public void testGetAll() throws NoSuchResourceException {
		imageDAO.create(BYTE_ARR, createEvent());
		imageDAO.create(BYTE_ARR, createEvent());
		imageDAO.create(BYTE_ARR, createEvent());

		List<ImageEntity> all = imageDAO.getAll();

		assertFalse(all.isEmpty());
		assertEquals(3, all.size());
	}

	@Test
	public void testDelete() throws NoSuchResourceException {
		ImageEntity entity = imageDAO.create(BYTE_ARR, createEvent());
		imageDAO.delete(entity.getId());
	}

	@Test(expected = NoSuchResourceException.class)
	public void testDeleteNotExistingResource() throws NoSuchResourceException {
		imageDAO.delete("1");
	}

	@Test(expected = NoSuchResourceException.class)
	public void testGetNotExistingResource() throws NoSuchResourceException {
		imageDAO.getById("1");
	}

	private ImageUploadEvent createEvent() {
		ImageUploadEvent imageUploadEvent = new ImageUploadEvent();
		imageUploadEvent.setName(FILE_NAME_JPG);
		imageUploadEvent.setCaption(CAPTION);
		imageUploadEvent.setUseNameAltAndCaption(FILE_NAME_AS_ALT);
		imageUploadEvent.setAltTag(ALT_TAG);
		return imageUploadEvent;
	}

}
